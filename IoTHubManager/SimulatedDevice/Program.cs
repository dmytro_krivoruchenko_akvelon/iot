﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Devices.Client;
using Newtonsoft.Json;

namespace SimulatedDevice
{
    class Program
    {
        static DeviceClient deviceClient;
        static string iotHubUri = "IoTHub2ForIntruderAlert.azure-devices.net";
        static string deviceKey = "6kxSXq9Pf5nuXVX3LPlzAeUdCD93UPKe8NxF5GHJ92g=";
        //static string deviceKey = "L7H4DpNHBE7OQoUEMix/zra2IojrExw7Um0tFgRuI1g=";
        // static string connectionString = "HostName=mssworda0fa2ce36d854dd08918be3ea62cca9f.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=WmMuNZhGGEQQQFhHILTc4IwmicvhkjLUoJeoYzadd1I=";
        //l8YgAWl6DT1oIdn6luGeWrGxG7KRUEFG0N2ukm448uM=
        static void Main(string[] args)
        {
            Console.WriteLine("Simulated device\n");
            deviceClient = DeviceClient.Create(iotHubUri, new DeviceAuthenticationWithRegistrySymmetricKey("Device1B19F1R1", deviceKey), TransportType.Mqtt);
            //deviceClient = DeviceClient.Create(iotHubUri, new DeviceAuthenticationWithRegistrySymmetricKey("Device2B22F2R2", deviceKey), TransportType.Mqtt);
            deviceClient.ProductInfo = "HappyPath_Simulated-CSharp";
            SendDeviceToCloudMessagesAsync();
            Console.ReadLine();
        }

        private static async void SendDeviceToCloudMessagesAsync()
        {
            int messageId = 1;
            Random rand = new Random();
            bool flag = true;
            while (flag)
            {
                var telemetryDataPoint = new
                {
                    DeviceId = "Device1B19F1R1",
                    Subject = "Intruder Alert",
                    Building = "B13",
                    Floor = "F2",
                    Room = "R3",
                   Time = "2018-05-13T07:07:07Z"
                };
                var messageString = JsonConvert.SerializeObject(telemetryDataPoint);
                var message = new Message(Encoding.ASCII.GetBytes(messageString));

                await deviceClient.SendEventAsync(message);
                Console.WriteLine("{0} > Sending message: {1}", DateTime.Now, messageString);

                await Task.Delay(1000);
            }
        }
    }
}
